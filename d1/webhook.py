import logging

log = logging.getLogger(__name__)


async def webhook_send(ctx, err, **kwargs):
    """Send a webhook showing an error."""
    # generate an embed based on error and kwargs

    domain_id, domain = kwargs.get("domain", (None, None))

    embed = {
        "title": f"`{domain}`, ID {domain_id}",
        "footer": {"text": "beep boop",},
        "color": 0xFF0000,
        "description": f"{err.description}: **{err.message}**",
        "fields": [{"name": "possible cause", "value": err.possible_cause,}],
    }

    full_payload = {"content": "d1 error", "embeds": [embed]}

    # use ctx.session to send a webhook
    async with ctx.session.post(ctx.webhook_url, json=full_payload) as resp:
        log.info("webhook res: %d", resp.status)
